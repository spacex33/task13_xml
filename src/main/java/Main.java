import parser.Parser;
import parser.TourSAXParser;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

//        Parser parser = new TourDOMParser();
        Parser parser = new TourSAXParser();
        File xml = new File("src\\main\\resources\\tours.xml");
        System.out.println(parser.parse(xml));
    }
}
